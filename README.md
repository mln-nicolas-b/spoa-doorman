# spoa-doorman

A simple, minimalist solution for limiting the number of requests on a HAProxy backend

## Overview

SPOA-DOORMAN is an agent for HAProxy to filter and ban source IP addresses that would violate a certain threshold of requests 
over a given time.  
It is based on the time series functionality provided by the Redis database.  

1. Users send requests to HAProxy
2. HAProxy send a message to its agent, including the source IP address, the host and the path
3. The SPOA makes database queries to determine if the source IP address should be banned, 
   then creates or updates the IP address score.
4. The SPOA looks up the IP address score
5. The SPOA returns the score of the IP address to the HAProxy
6. HAProxy routes traffic to the backend application in case the ACLs in its configuration match the returned score

<div style="text-align:center"><img src="./docs/assets/spoa-doorman.png"/></div>

## Installation

### Development

Simply launch the stack with docker compose :

```bash
docker-compose up -d
```

If needed, reconpile the docker image :

```bash
docker-compose up -d --build
```

## Configuration

**Warning**: The reading of configuration files by HaProxy can be capricious.
Each configuration file must end with an empty line to avoid errors that could stop the HaProxy daemon.

### HaProxy

The SPOE is defined in the file ***/etc/haproxy/spoe-doorman.conf*** :

* `messages` : *Name of the message to be sent to the agent. The agent expects to receive a certain message, 
  if it does not receive a message it recognizes, then no processing will be done*
* `option var-prefix` : *Primordial in the reject condition to retrieve the score value and thus play a filtering rule*
* `use-backend` : *Use of the backend declared in HaProxy to connect to the agent*
* `args` : *Argments to send to provide the agent with the necessary information for its processing*
* `event` : *Event on which a treatment will be operated*

```haproxy
    [doorman]
    spoe-agent doorman-agent
        messages     check-doorman
        option       var-prefix  doorman
        timeout      hello       100ms
        timeout      idle        30s
        timeout      processing  1s
        use-backend  spoe-doorman
    spoe-message check-doorman
        args         ip=src path=path req=req.hdr(host)
        event        on-frontend-http-request

```

The configurations in the spoe file will be used to instantiate a spoe engine in the Haproxy global configuration file :

* Set up a backend `backend spoe-doorman` that will be used in the SPOE configuration to connect to the agent
* Set up a backend for the application and a frontend that uses the same backend
* Declaration of a new filter `filter spoe engine doorman config /etc/haproxy/spoe-doorman.conf` which refers to 
  the agent pool *doorman* declared earlier and contained in the file ***/etc/haproxy/spoe-doorman.conf***.
* Setting up the rule `http-request deny if { var(sess.doorman.score) -m int eq 0 }`. 
  This one rejects all banned IP with a score of 0 (returned by the SPOA agent).
* Setting up the rule `http-request deny if { var(sess.doorman.score) -m int lt 50 }`.
  This one rejects any connection whose score (returned by the SPOA agent) would be lower than 50.
  The score can be changed according to needs

```haproxy
global
    log stdout format raw local0
    maxconn 1024

defaults
    log global
    timeout client 60s
    timeout connect 60s
    timeout server 60s

frontend fe-app
    bind *:80
    mode http
    option httplog

    filter spoe engine doorman config /usr/local/etc/haproxy/spoe-doorman.conf

    # Block all banned IP
    http-request deny if { var(sess.doorman.score) -m int eq 0 }
    
    # Setting the minimum threshold of the reputation score to 50
    http-request deny if { var(sess.doorman.score) -m int lt 50 }

    use_backend app

backend app
    mode http
    server app1 172.30.0.11:80

backend spoe-doorman
    mode tcp
    server doorman-spoa1 172.30.0.13:12400

```

### SPOA

The agent provides several configuration methods.

#### Configuration file

File-based configuration is the first method. It is also the weakest, since it can be overloaded by all other methods.

```yaml
spoa:
  host: "0.0.0.0"                   # Listen Address
  port: "12400"                     # Listen port

redis:
  host: "127.0.0.1"                 # Redis server address or domain
  port: "6379"                      # Redis server port
  index: "0"                        # Redis database index
  auth: 
    user: ""                        # Redis authentication user
    password: ""                    # Redis authentication password

jail:
  default:                          # Default configuration. Match even customs configurations unmatch
    range: 60s                      # Time range onto check the sum of requests
    holding: 0s                     # Time to add to the time range to keep clients denied
    count: 5                        # Number of requests by default to denied client
  customs:                          # Customs configurations. Override the default configuration in the event of a match
    - host: test.domain.tld         # Host to match
      range: 60s                    # Time range onto check the sum of requests
      holding: 0s                   # Time to add to the time range to keep clients denied
      targets:                      # List of path to manage
        - request: /                # Path requested
          count: 5                  # Number of requests by default to denied client
        - request: /test-1
          count: 10
        - request: /test-1/test-2
          count: 15

log:
  path: "/var/log/doorman.log"      # Path of the log file
```

The configuration file is automatically read from the following directories :
* /etc/doorman
* /etc/
* $HOME
* ./

The configuration file must be named ***doorman.yaml*** and formatted with the YAML syntax

Custom configurations can only be declared from the configuration file.

#### Environment variables

Environment variable configuration is the second method.  
It is particularly useful in a container environment, but can be overloaded by command-line options.

* `DOORMAN_CONFIG` **type *string*** - Config file path
* `DOORMAN_HOST` **type *string*** - SPOA listening address
* `DOORMAN_JAIL_DEFAULT_COUNT` **type *int*** - Number of requests required by default to denied client
* `DOORMAN_JAIL_DEFAULT_HOLDING` **type *string*** - Time to add to the time range to keep clients denied
* `DOORMAN_JAIL_DEFAULT_RANGE` **type *string*** - Time range over which to perform the control
* `DOORMAN_LOG_PATH` **type *string*** - Path of the log file
* `DOORMAN_PORT` **type *string*** - SPOA port
* `DOORMAN_REDIS_AUTH_PASSWORD` **type *string*** - Redis authentication password
* `DOORMAN_REDIS_AUTH_USER` **type *string*** - Redis authentication user
* `DOORMAN_REDIS_HOST` **type *string*** - Redis database host or IP
* `DOORMAN_REDIS_INDEX` **type *string*** - Redis database index
* `DOORMAN_REDIS_PORT` **type *string*** - Redis database port

#### Command line arguments

The last method is via command line arguments.  
This method takes precedence over all others and will therefore override the other configurations.

* `--config` **type *string*** - Config file path
* `--host` **type *string*** - SPOA listening address
* `--jail.default.count` **type *int*** - Number of requests required by default to denied client
* `--jail.default.holding` **type *string*** - Time to add to the time range to keep clients denied
* `--jail.default.range` **type *string*** - Time range over which to perform the control
* `--log.path` **type *string*** - Path of the log file
* `--port` **type *string*** - SPOA port
* `--redis.auth.password` **type *string*** - Redis authentication password
* `--redis.auth.user` **type *string*** - Redis authentication user
* `--redis.host` **type *string*** - Redis database host or IP
* `--redis.index` **type *string*** - Redis database index
* `--redis.port` **type *string*** - Redis database port

## Test

To test, use the file ***run.sh*** present in the directory ***configs/client/***.
Comment or uncomment the wanted `curl` command or create custom commands.

The client container will send requests autonomously

