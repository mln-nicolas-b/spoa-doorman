package middlewares

import (
	"fmt"
	"regexp"
	"strconv"
)

func TimeElapsed(time, start, end string) (bool, error) {
	s, err := strconv.ParseInt(start, 10, 64)
	if err != nil {
		return false, fmt.Errorf("failed to format the starting timestamp to integer: %s", err)
	}

	e, err := strconv.ParseInt(end, 10, 64)
	if err != nil {
		return false, fmt.Errorf("failed to format the ending timestamp to integer: %s", err)
	}

	elapsed := s - e

	spc, err := TimeConverter(time)
	if err != nil {
		return false, err
	}

	if elapsed > spc {
		return false, nil
	}

	return true, nil
}

func TimeConverter(t string) (int64, error) {
	// Get the time unit of the configuration
	reUnit := regexp.MustCompile(`[0-9]+`)
	u := reUnit.Split(t, -1)

	// Get the interger value of the configuration
	reValue := regexp.MustCompile(`[a-zA-Z]+`)
	v := reValue.Split(t, -1)

	valInt, err := strconv.ParseInt(v[0], 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to format the holding time to integer: %s", err)
	}

	switch u[1] {
	case "s":
		return valInt * 1000000000, nil // Convert seconds to miliseconds
	case "m":
		return valInt * 60000000000, nil // Convert minutes to miliseconds
	case "h":
		return valInt * 3600000000000, nil // Convert hours to miliseconds
	case "d":
		return valInt * 864000000000000, nil // Convert days to miliseconds
	default:
		return 0, fmt.Errorf("failed to format return a converted time")
	}

}
