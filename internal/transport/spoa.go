package config

import (
	"fmt"
)

type Spoa struct {
	Host    string `mapstructure:"host"`
	Port    string `mapstructure:"port"`
	Message Message
}

type Message struct {
	IP   string
	Path string
	Host string
	URL  string
}

func (s *Spoa) ListeningAddress() string {
	return fmt.Sprintf("%s:%s", s.Host, s.Port)
}
