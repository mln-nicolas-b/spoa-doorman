package app

import (
	"fmt"
	"net"

	cfg "gitlab.com/mln-nicolas-b/spoa-doorman/internal/config"
	"go.uber.org/zap"

	spoe "github.com/criteo/haproxy-spoe-go"
)

func SPOA() {
	var C cfg.Config
	var err error

	if err = C.Load(); err != nil {
		panic(err)
	}

	logger := C.Log.NewLogger()

	err = C.Redis.NewClient()
	if err != nil {
		logger.Sugar().Error(err)
	}

	// Create the agent processing
	agent := spoe.New(func(messages *spoe.MessageIterator) ([]spoe.Action, error) {
		for messages.Next() {
			// Check sent message
			msg := messages.Message
			if msg.Name != "check-doorman" {
				logger.Error("Failed to match with the message sent")
				continue
			}

			var ip net.IP
			var ok bool
			var path, req string

			for msg.Args.Next() {
				arg := msg.Args.Arg

				if arg.Name == "ip" {
					ip, ok = arg.Value.(net.IP)
					if !ok {
						return nil, fmt.Errorf("bad IP format in message")
					} else {
						C.SPOA.Message.IP = ip.String()
					}
				}

				if arg.Name == "path" {
					path, ok = arg.Value.(string)
					if !ok {
						return nil, fmt.Errorf("bad path format in message")
					} else {
						C.SPOA.Message.Path = path
					}
				}

				if arg.Name == "req" {
					req, ok = arg.Value.(string)
					if !ok {
						return nil, fmt.Errorf("bad req format in message")
					} else {
						C.SPOA.Message.Host = req
					}
				}
			}

			C.SPOA.Message.URL = C.SPOA.Message.Host + C.SPOA.Message.Path

			for _, rules := range C.Jail.Custom {
				if rules.Host == C.SPOA.Message.Host {
					err := C.Jail.DefineRules("custom", C.SPOA.Message.Path, rules)
					if err != nil {
						logger.Sugar().Error(err)
					}
				} else {
					err := C.Jail.DefineRules("default", C.SPOA.Message.Path, rules)
					if err != nil {
						logger.Sugar().Error(err)
					}
				}
			}

			if err := C.Redis.Post(C.SPOA.Message, C.Jail); err != nil {
				logger.Sugar().Error(err)
			}
		}

		if err := C.Redis.Jail(C.SPOA.Message, C.Jail); err != nil {
			logger.Sugar().Error(err)
		}

		score, err := C.Redis.Lookup(C.SPOA.Message.IP)
		if err != nil {
			logger.Sugar().Error(err)
		}

		// Log values
		logger.Info(
			"New request",
			zap.String("source", C.SPOA.Message.IP),
			zap.String("request", C.SPOA.Message.URL),
			zap.Int("score", score),
		)

		// Return values to Haproxy
		return []spoe.Action{
			spoe.ActionSetVar{
				Name:  "score",
				Scope: spoe.VarScopeSession,
				Value: score,
			},
		}, nil
	})

	// Launch the SPOA agent server
	logger.Info(C.SPOA.ListeningAddress())
	if err := agent.ListenAndServe(C.SPOA.ListeningAddress()); err != nil {
		logger.Error(fmt.Sprintf("%v", err))
	}
}
