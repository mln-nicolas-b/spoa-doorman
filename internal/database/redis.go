package database

import (
	"context"
	"fmt"
	"strconv"
	"time"

	redistimeseries "github.com/RedisTimeSeries/redistimeseries-go"
	redisV9 "github.com/redis/go-redis/v9"

	svcs "gitlab.com/mln-nicolas-b/spoa-doorman/internal/services"
	tspt "gitlab.com/mln-nicolas-b/spoa-doorman/internal/transport"
)

const (
	keyname string = "doorman"
)

var (
	ctx = context.Background()
)

type Redis struct {
	Host  string    `mapstructure:"host"`
	Port  string    `mapstructure:"port"`
	Index string    `mapstructure:"index"`
	Auth  RedisAuth `mapstructure:"auth"`

	DB *redisV9.Client
	TS *redistimeseries.Client
}

type RedisAuth struct {
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

func (r *Redis) NewClient() error {
	redisAddr := fmt.Sprintf("%s:%s", r.Host, r.Port)

	clientURL := fmt.Sprintf("redis://%s:%s@%s/%s", r.Auth.User, r.Auth.Password, redisAddr, r.Index)
	opt, err := redisV9.ParseURL(clientURL)
	if err != nil {
		return err
	}

	// Configure the database connections
	r.DB = redisV9.NewClient(opt)
	r.TS = redistimeseries.NewClient(redisAddr, "doorman", nil)

	return nil
}

func (r *Redis) Post(msg tspt.Message, jail svcs.Jail) (err error) {
	lbl := map[string]string{}

	createOptions := redistimeseries.CreateOptions{
		// Uncompressed: true,
		RetentionMSecs: time.Duration(jail.Rules.Range),
		Labels:         lbl,
		// DuplicatePolicy: r.DPT,
	}

	// key := fmt.Sprintf("%s:%s:%s", keyname, msg.IP, msg.URL)
	key := fmt.Sprintf("%s:%s", keyname, msg.IP)
	_, err = r.TS.AddAutoTsWithOptions(key, 0, createOptions)
	if err != nil {
		return fmt.Errorf("failed to post new mesure: %v", err)
	}

	return nil
}

// Jail check if source IP is to ban according specific configuration value
func (r *Redis) Jail(msg tspt.Message, jail svcs.Jail) error {
	// key := fmt.Sprintf("%s:%s:%s", keyname, msg.IP, msg.URL)
	key := fmt.Sprintf("%s:%s", keyname, msg.IP)

	dp, err := r.TS.Get(key)
	if err != nil {
		return err
	}

	fromTimestamp := dp.Timestamp - jail.Rules.Range
	toTimestamp := dp.Timestamp

	rwo := redistimeseries.RangeOptions{
		Count: int64(jail.Rules.Count * 2),
	}

	rg, err := r.TS.RangeWithOptions(key, fromTimestamp, toTimestamp, rwo)
	if err != nil {
		return fmt.Errorf("failed to range over the key")
	}

	if len(rg) >= jail.Rules.Count {
		r.DB.Set(ctx, msg.IP, 0, time.Duration(jail.Rules.Holding+jail.Rules.Range))
	} else {
		r.DB.Set(ctx, msg.IP, 100, time.Duration(jail.Rules.Holding))
	}

	return nil
}

func (r *Redis) Lookup(ip string) (int, error) {
	value, err := r.DB.Get(ctx, ip).Result()
	if err != nil {
		if fmt.Sprintf("%v", err) == "redis: nil" {
			return 100, nil
		} else {
			return 100, fmt.Errorf("failed to get the key from database")
		}
	}

	score, err := strconv.Atoi(value)
	if err != nil {
		return 100, fmt.Errorf("failed to convert the value to interger score")
	}
	return score, nil
}
