package config

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Log struct {
	Path string `mapstructure:"path"`
}

func (l *Log) NewLogger() *zap.Logger {
	file, err := os.OpenFile(l.Path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	cfg := zap.NewProductionConfig()
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(cfg.EncoderConfig),
		zapcore.NewMultiWriteSyncer(os.Stdout, file),
		zapcore.InfoLevel,
	)

	logger := zap.New(core)
	logger.Sync()

	return logger
}
