package config

import (
	"os"
	"strings"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	d "gitlab.com/mln-nicolas-b/spoa-doorman/internal/database"
	s "gitlab.com/mln-nicolas-b/spoa-doorman/internal/services"
	t "gitlab.com/mln-nicolas-b/spoa-doorman/internal/transport"
)

type Config struct {
	SPOA  t.Spoa  `mapstructure:"spoa"`
	Redis d.Redis `mapstructure:"redis"`
	Log   Log     `mapstructure:"log"`
	Jail  s.Jail  `mapstructure:"jail"`
}

func (c *Config) Load() (err error) {
	// Set configuration file
	configFilePath := pflag.String("config", "", "Config file path")

	// Set spoa
	c.SPOA.Host = returnStringValue("host", "", "SPOA listening address")
	c.SPOA.Port = returnStringValue("port", "", "SPOA port")

	// Set Database
	c.Redis.Host = returnStringValue("redis.host", "", "Redis database host or IP")
	c.Redis.Port = returnStringValue("redis.port", "", "Redis database port")
	c.Redis.Index = returnStringValue("redis.index", "", "Redis database index")
	c.Redis.Auth.User = returnStringValue("redis.auth.user", "", "Redis authentication user")
	c.Redis.Auth.Password = returnStringValue("redis.auth.password", "", "Redis authentication password")

	// Set bans
	c.Jail.Default.Range = returnStringValue("jail.default.range", "", "Time range over which to perform the control")
	c.Jail.Default.Holding = returnStringValue("jail.default.holding", "", "Time to add to the time range to keep clients denied")
	c.Jail.Default.Count = returnInt64Value("jail.default.count", 0, "Number of requests required by default to denied client")

	// Set Logs
	c.Log.Path = returnStringValue("log.path", "", "Path of the log file")

	pflag.Parse()

	viper.BindPFlags(pflag.CommandLine)

	if len(*configFilePath) == 0 {
		viper.SetConfigName("doorman.yaml")
		viper.SetConfigType("yaml")
		viper.AddConfigPath("/etc/doorman")
		viper.AddConfigPath("/etc/")
		viper.AddConfigPath("$HOME/")
		viper.AddConfigPath(".")
	} else {
		if _, err = os.Stat(*configFilePath); err == nil {
			viper.SetConfigFile(*configFilePath)
			viper.SetConfigType("yaml")
		} else {
			return err
		}
	}

	err = viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(c)
	if err != nil {
		return err
	}

	viper.AutomaticEnv()

	return nil
}

func returnStringValue(name, value, usage string) string {
	str := pflag.String(name, value, usage)
	mapEnv(name)
	return *str
}

func returnInt64Value(name string, value int, usage string) int {
	int := pflag.Int(name, value, usage)
	mapEnv(name)
	return *int
}

func mapEnv(key string) {
	envKey := strings.Replace(strings.ToUpper(key), ".", "_", -1)
	viper.BindEnv(key, "DOORMAN_"+envKey)
}
