package config

import (
	"strings"

	"gitlab.com/mln-nicolas-b/spoa-doorman/internal/middlewares"
)

type Jail struct {
	Default Defaults `mapstructure:"defaults"`
	Custom  []Custom `mapstructure:"custom"`
	Rules   Rules
}

type Defaults struct {
	Range   string `mapstructure:"range"`
	Holding string `mapstructure:"holding"`
	Count   int    `mapstructure:"count"`
}

type Custom struct {
	Host    string    `mapstructure:"host"`
	Range   string    `mapstructure:"range"`
	Holding string    `mapstructure:"holding"`
	Targets []Targets `mapstructure:"targets"`
}

type Targets struct {
	Request string `mapstructure:"request"`
	Count   int    `mapstructure:"count"`
}

type Rules struct {
	Range   int64
	Holding int64
	Count   int
}

func (j *Jail) DefineRules(mode, path string, c Custom) (err error) {
	// {test.domain.tld 60s 0 0s 0 [{/ 5} {/test-1 10} {/test-1/test-2 15}]}
	switch mode {
	case "default":
		j.Rules.Range, err = middlewares.TimeConverter(j.Default.Range)
		if err != nil {
			return err
		}
		j.Rules.Holding, err = middlewares.TimeConverter(j.Default.Holding)
		if err != nil {
			return err
		}
		j.Rules.Count = j.Default.Count
	case "custom":
		maxPrefixLength := -1

		j.Rules.Range, err = middlewares.TimeConverter(c.Range)
		if err != nil {
			return err
		}
		j.Rules.Holding, err = middlewares.TimeConverter(c.Holding)
		if err != nil {
			return err
		}

		for _, req := range c.Targets {
			if strings.HasPrefix(path, req.Request) && len(req.Request) > maxPrefixLength {
				maxPrefixLength = len(req.Request)
				j.Rules.Count = req.Count
			}
		}
	}
	return nil
}
